package http_servefile

import (
	"log"
	"net/http"
	"os"
	"strconv"
)

func handleError(text string, code int, w http.ResponseWriter) {
	w.WriteHeader(code)
	if _, ok := w.Header()["Content-Length"]; ok {
		w.Header().Set("Content-Length", strconv.Itoa(len(text)))
	} else {
		w.Header().Add("Content-Length", strconv.Itoa(len(text)))
	}
	if _, ok := w.Header()["Content-Type"]; ok {
		w.Header().Set("Content-Type", "text/html;charset=utf-8")
	} else {
		w.Header().Add("Content-Type", "text/html;charset=utf-8")
	}
	w.Write([]byte(text))
}
func handle404(w http.ResponseWriter) {
	text := "file not found"
	handleError(text, 404, w)
}
func handle403(w http.ResponseWriter) {
	text := "file access forbided"
	handleError(text, 403, w)
}
func ServeFile(directory string, w http.ResponseWriter, req *http.Request) {
	path := req.URL.Path
	if path == "/" {
		path = directory + "/index.html"
	} else {
		path = directory + path
	}
	f, err := os.Open(path)
	log.Println(path)
	defer f.Close()
	if os.IsNotExist(err) {
		//404
		handle404(w)
		return
	}
	state, _ := os.Stat(path)
	if state.IsDir() {
		//403
		handle403(w)
		return
	}
	//200
	http.ServeFile(w, req, path)
	return
}
