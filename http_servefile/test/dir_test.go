package test

import (
	"os"
	"testing"
)

func TestExist(t *testing.T) {
	f, err := os.Open("./testd")
	if !os.IsNotExist(err) {
		t.Fatal("dir unexist, but check failed")
	}
	defer f.Close()
	state, _ := os.Stat("./empty_dir")
	if !state.IsDir() {
		t.Fatal("this is a dir")
	}
}
