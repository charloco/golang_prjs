package main

import (
	"net/http"

	"gitlab.com/charloco/golang_prjs/http_servefile"
)

func handler(w http.ResponseWriter, req *http.Request) {
	http_servefile.ServeFile("./test", w, req)
}
func main() {
	server := http.Server{
		Addr:    ":18000",
		Handler: http.HandlerFunc(handler),
	}
	server.ListenAndServe()
}
